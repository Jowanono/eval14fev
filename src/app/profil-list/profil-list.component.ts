import {Component, Input, OnInit} from '@angular/core';
import {ProfilModel} from '../model/profil.model';
import {ApiZinderService} from '../service/apiZinder.service';
import {InteretModel} from '../model/interet.model';

@Component({
  selector: 'app-profil-list',
  templateUrl: './profil-list.component.html',
  styleUrls: ['./profil-list.component.css']
})
export class ProfilListComponent implements OnInit {
  @Input() profils: ProfilModel[] = [];

  constructor(
    private apiZinder: ApiZinderService
  ) { }

  ngOnInit() {
    this.apiZinder.getProfilList().subscribe(pList => {
      console.log(pList);
      this.profils = Object.values(pList);
      console.log(this.profils);
    });
  }

  getInteretName(interet: InteretModel) {
  }

}
