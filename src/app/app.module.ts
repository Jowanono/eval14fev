import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ProfilComponent} from './profil/profil.component';
import {ProfilListComponent} from './profil-list/profil-list.component';
import {RouterModule, Routes} from '@angular/router';
import {StatistiquesComponent} from './statistiques/statistiques.component';
import {AccueilComponent} from './accueil/accueil.component';
import {HttpClientModule} from '@angular/common/http';
import {ApiZinderService} from './service/apiZinder.service';
import { MatchComponent } from './match/match.component';
import { AdministrationComponent } from './administration/administration.component';
import {FormsModule} from "@angular/forms";

const appRoutes: Routes = [
  {path: 'stats', component: StatistiquesComponent},
  {path: 'admin', component: AdministrationComponent},
  {path: '', component: AccueilComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    ProfilListComponent,
    StatistiquesComponent,
    AccueilComponent,
    MatchComponent,
    AdministrationComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule,
    RouterModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ApiZinderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
