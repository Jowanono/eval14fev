import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiZinderService} from '../service/apiZinder.service';
import {NewMatchModel} from '../model/newMatch.model';
import {MatchModel} from '../model/match.model';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {
  @Input() id: string;
  @Output() matchCreated = new EventEmitter<MatchModel>();

  constructor(
    private apiZinder: ApiZinderService
  ) {
  }

  ngOnInit() {
  }

  createMatch(id, match: boolean) {
    this.apiZinder.doMatch(id, new NewMatchModel(match)).subscribe(reponse => {
      console.log(reponse);
      return this.matchCreated.emit(reponse);
    });
  }
}
