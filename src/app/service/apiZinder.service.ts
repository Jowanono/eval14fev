import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ProfilModel} from '../model/profil.model';
import {map} from 'rxjs/operators';
import {ProfilListModel} from '../model/profilList.model';
import {NewMatchModel} from '../model/newMatch.model';
import {MatchModel} from '../model/match.model';
import {NewProfilModel} from "../model/newProfil.model";

@Injectable()
export class ApiZinderService {

  constructor(
    private http: HttpClient
  ) {
  }

  getProfilList(): Observable<ProfilModel []> {
    return this.http.get<ProfilListModel>('http://localhost:8088/zinder/profils')
      .pipe(map(listOfProfil => listOfProfil.profils));
  }

  doMatch(id: string, match: NewMatchModel): Observable<MatchModel> {
    return this.http.post<MatchModel>('http://localhost:8088/zinder/profils/' + id
      + '/match', match);
  }

  getMatches(): Observable<MatchModel> {
    return this.http.get<MatchModel>('http://localhost:8088/zinder/matchs');
  }

  deleteMatch(id: string) {
    return this.http.delete('http://localhost:8088/zinder/matchs/' + id);
  }

  createProfil(nouveauProfil: NewProfilModel): Observable<ProfilModel> {
    return this.http.post<ProfilModel>('http://localhost:8088/zinder/profils',
      nouveauProfil);
  }
}
