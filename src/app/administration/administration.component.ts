import {Component, OnInit} from '@angular/core';
import {ApiZinderService} from '../service/apiZinder.service';
import {NewProfilModel} from '../model/newProfil.model';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})
export class AdministrationComponent implements OnInit {
  prenom: string;
  nom: string;
  url: string;
  hobbie1: string;
  hobbie2: string;
  hobbie3: string;
  hobbie4: string;

  constructor(
    private apiZinder: ApiZinderService
  ) { }

  ngOnInit() {
  }

  createProfil() {
    const nouveauProfil = new NewProfilModel(this.nom, this.prenom,
      this.url, [this.hobbie1, this.hobbie2, this.hobbie3, this.hobbie4]);
    this.apiZinder.createProfil(nouveauProfil).subscribe( np => {
      console.log(np);
    });
  }

}
