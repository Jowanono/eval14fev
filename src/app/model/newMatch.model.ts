export class NewMatchModel {
  match: boolean;

  constructor(
    match: boolean
  ) {
    this.match = match;
  }
}
