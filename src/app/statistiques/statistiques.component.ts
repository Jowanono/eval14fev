import {Component, OnChanges, OnInit} from '@angular/core';
import {ApiZinderService} from '../service/apiZinder.service';
import {MatchModel} from '../model/match.model';

@Component({
  selector: 'app-statistiques',
  templateUrl: './statistiques.component.html',
  styleUrls: ['./statistiques.component.css']
})
export class StatistiquesComponent implements OnInit, OnChanges {
  matchList: MatchModel[] = [];

  constructor(
    private apiZinder: ApiZinderService
  ) { }

  ngOnInit() {
    this.getMatches();
  }

  ngOnChanges() {
    this.getMatches();
  }

  getMatches() {
    this.apiZinder.getMatches().subscribe(mList => {
      console.log(mList);
      this.matchList = Object.values(mList);
    });
  }


  deleteMatch(id: string) {
    this.apiZinder.deleteMatch(id).subscribe(delMatch => {
      console.log(delMatch);
    });
  }
}
